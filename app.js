
var compact = VueColor.Compact;
//var slider = sliderComponent;

var dmxFixture = Vue.extend({
	template: '#dmx-fixture-4channels',
	components: {
      'compact-picker': compact,
      //'slider': slider
    },
	props: ['name', 'mqttClient'],
	data: function() {
		return {
			colors: {
        		hex: '#FF0000',
        		hsl: {h: 0, s: 0, l: 0, a: 1},
        		hsv: {h: 0, s: 0, v: 0, a: 1},
        		rgba: {r: 255, g: 0, b: 0, a: 1},
        		a: 1
      		},
      		dimming: 50
		};
	},
	methods: {
		onChange_dimming: function(val)
		{
			
			this.onChange_colors(this.colors);
			//this.dimming = val;
		},
		onChange_colors: function(val)
		{
			this.colors = val;        
       		message = new Paho.MQTT.Message(
       			String(this.colors.rgba.r) + "-" +
        		String(this.colors.rgba.g) + "-" +
        		String(this.colors.rgba.b) + "-" +
        		String(this.dimming));
        	message.destinationName = "dmx/A/" + this.name + "/";
        	this.$parent.mqttClient.send(message);        
		}

	}
});

new Vue({
    el: '#dmx',
    components: {
		'dmx-fixture' : dmxFixture    
    },     
    data: {    
      mqttClient: '',
      mqttURL: 'localhost',
      mqttPort: '1884',
      mqttConnected: false
    },
    methods: {

      connectMQTT() {
        function onMQTTConnectionSucceeded (x) {x.invocationContext.mqttConnected = true};
        function onMQTTConnectionFailed (x) {x.invocationContext.mqttConnected = false};
        function onMQTTConnectionLost (x) {console.log(x)};
        
        var r = Math.round(Math.random()*Math.pow(10,5));
        var d = new Date().getTime();
        var cid = r.toString() + "-" + d.toString();
        this.mqttClient = new Paho.MQTT.Client(this.mqttURL, Number(this.mqttPort), cid );
        this.mqttClient.onConnectionLost = onMQTTConnectionLost;
        this.mqttClient.connect({invocationContext: this, onSuccess: onMQTTConnectionSucceeded, onFailure: onMQTTConnectionFailed});        
      },
      disconnectMQTT() {
      	this.mqttClient.disconnect();
      	this.mqttConnected = false;
      }
    },

});
